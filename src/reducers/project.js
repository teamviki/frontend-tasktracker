import actionTypes  from '../actions/actionTypes';

const INITIAL_STATE = {
    projectList: [],
    project: null,
    loading: false,
    error: null,
    success: null,
};

/**
* @param {Object} state - Default application state
* @param {Object} action - Action from action creator
* @returns {Object} New state
*/
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.fetchProjectListInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.fetchProjectListError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null,
                projectList: []

            };
        case actionTypes.fetchProjectListSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
                projectList: action.payload,
            };
        //Project Detail Fetch
        case actionTypes.fetchProjectInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.fetchProjectError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.fetchProjectSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
            };

        // Project Creation
        case actionTypes.createProjectInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.createProjectError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null,
            };
        case actionTypes.createProjectSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
            };
        // Project Update
        case actionTypes.updateProjectInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.updateProjectError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.updateProjectSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
            };

        // Project Delete
        case actionTypes.deleteProjectInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.deleteProjectError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.deleteProjectSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
            };

        case actionTypes.hideSnackError :
            return {
                    ...state,
                    error: null,
                    loading: false,
                    success: null,
                };
        default: return state;
    }
};