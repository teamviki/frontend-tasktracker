import { combineReducers } from "redux";

import app from './app';
import project from './project';
import task from './task';



export const rootReducer = combineReducers({app, project, task})
