import actionTypes  from '../actions/actionTypes';

const INITIAL_STATE = {
    taskList: [],
    task: null,
    loading: false,
    error: null,
    success: null,
};

/**
* @param {Object} state - Default application state
* @param {Object} action - Action from action creator
* @returns {Object} New state
*/
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.fetchTaskListInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.fetchTaskListError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.fetchTaskListSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
                taskList: action.payload,
            };
        //Project Detail Fetch
        case actionTypes.fetchTaskInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.fetchTaskError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.fetchTaskSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
                task: action.task
            };

        // Project Creation
        case actionTypes.createTaskInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.createTaskError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.createTaskSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
            };
        // Project Update
        case actionTypes.updateTaskInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.updateTaskError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.updateTaskSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
            };

        // Project Delete
        case actionTypes.deleteTaskInit:
            return {
                ...state,
                error: null,
                loading: true,
                success: null
            };
        case actionTypes.deleteTaskError:
            return {
                ...state,
                error: action.error,
                loading: false,
                success: null
            };
        case actionTypes.deleteTaskSuccess:
            return {
                ...state,
                error: null,
                loading: false,
                success: true,
            };

        case actionTypes.hideSnackError :
            return {
                    ...state,
                    error: null,
                    loading: false,
                    success: null,
                };
        default: return state;
    }
};