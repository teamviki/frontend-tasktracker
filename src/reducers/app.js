import actionTypes from  '../actions/actionTypes';

const initialState = {
    error: null,
    user: null,
    loading: false,
}

export default (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.fetchUserInit:
      return { ...state, user: null, loading: true, error: null };

    case actionTypes.fetchUserSuccess:
      return { ...state, user: action.payload, loading: false, error: null };

    case actionTypes.fetchUserError:
      return {
          ...state, loading: false, error: action.error
      }
    case 'HIDE_SNACK_ERROR':
      return { ...state, error: null ,loading: false};
    default:
      return state
  }
};
