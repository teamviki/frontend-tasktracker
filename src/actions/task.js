import actionTypes from './actionTypes';
import {API_ENDPOINT} from '../helpers/constants';
import { hideError, makeAuthenticatedGetCall, makeAuthenticatedPostCall, makeAuthenticatedDeleteCall, makeAuthenticatedPutCall } from './commonActions';
import { fetchProjectListData } from './project'

export const fetchTaskListInit = () => ({
    type: actionTypes.fetchTaskListInit
})

export const fetchTaskListError = (error) => ({
    type: actionTypes.fetchTaskListError,
    error: JSON.stringify(error.toString()),
})

export const fetchTaskListSuccess = (data) => ({
    type: actionTypes.fetchTaskListSuccess,
    payload: data,
})

export const fetchTaskListData  = (queryParam) => (dispatch) => {
   dispatch(fetchTaskListInit())
   return makeAuthenticatedGetCall(`${API_ENDPOINT}/tasks/${queryParam}`)
          .then(res => res.json())
          .then(json => {
			if(json.code === 200) {
				dispatch(fetchTaskListSuccess(json.response))
			} else {
				dispatch(fetchTaskListError(json.message))
				setTimeout(() => dispatch(hideError()), 2000);
            }
        }).catch(error => {
            dispatch(fetchTaskListError(error))
            setTimeout(() => dispatch(hideError()), 2000);

        })
};

export const createTaskInit = () => ({
    type: actionTypes.createTaskInit
})

export const createTaskError = (error) => ({
    type: actionTypes.createTaskError,
    error: JSON.stringify(error.toString()),
})

export const createTaskSuccess = (data) => ({
    type: actionTypes.createTaskSuccess,
    payload: data,
})

export const createNewTask = (formData) => dispatch => {
    dispatch(createTaskInit())
    return makeAuthenticatedPostCall(`${API_ENDPOINT}/task/create`, formData)
    .then(res => res.json())
    .then(json => {
        if(json.code === 200) {
            dispatch(createTaskSuccess(json.response))
            dispatch(fetchProjectListData())
        } else {
            dispatch(createTaskError(json.message))
            setTimeout(() => dispatch(hideError()), 2000);
        }
    }).catch(error => {
        dispatch(createTaskError(error))
        setTimeout(() => dispatch(hideError()), 2000);

    })
}


export const updateTaskInit = () => ({
    type: actionTypes.updateTaskInit
})

export const updateTaskError = (error) => ({
    type: actionTypes.updateTaskError,
    error: JSON.stringify(error.toString()),
})

export const updateTaskSuccess = (data) => ({
    type: actionTypes.updateTaskSuccess,
    payload: data,
})

export const updateExistingTask = (formData, id) => dispatch => {
    dispatch(updateTaskInit())
    return makeAuthenticatedPutCall(`${API_ENDPOINT}/task/update/${id}`, formData)
    .then(res => res.json())
    .then(json => {
        if(json.code === 200) {
            dispatch(updateTaskSuccess(json.response))
            dispatch(fetchProjectListData())
        } else {
            dispatch(updateTaskError(json.message))
            setTimeout(() => dispatch(hideError()), 2000);
        }
    }).catch(error => {
        dispatch(updateTaskError(error))
        setTimeout(() => dispatch(hideError()), 2000);

    })
}


export const deleteTaskInit = () => ({
    type: actionTypes.createTaskInit
})

export const deleteTaskError = (error) => ({
    type: actionTypes.deleteTaskError,
    error: JSON.stringify(error.toString()),
})

export const deleteTaskSuccess = (data) => ({
    type: actionTypes.deleteTaskSuccess,
    payload: data,
})

export const deleteExistingTask = (taskId) => dispatch => {
    dispatch(deleteTaskInit())
    return makeAuthenticatedDeleteCall(`${API_ENDPOINT}/task/delete/${taskId}`, )
    .then(res => res.json())
    .then(json => {
        if(json.code === 200) {
            dispatch(deleteTaskSuccess(json.response))
            dispatch(fetchProjectListData())
        } else {
            dispatch(deleteTaskError(json.message))
            setTimeout(() => dispatch(hideError()), 2000);
        }
    }).catch(error => {
        dispatch(deleteTaskError(error))
        setTimeout(() => dispatch(hideError()), 2000);

    })
}