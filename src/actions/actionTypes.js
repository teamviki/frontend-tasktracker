const actionTypes = { 
    hideError: 'HIDE_ERROR', 
    showLoader: 'SHOW_LOADER',
    hideSnackError: 'HIDE_SNACK_ERROR',


    fetchUserInit: 'FETCH_USER_INIT',
    fetchUserSuccess: 'FETCH_USER_SUCCESS',
    fetchUserError: 'FETCH_USER_ERROR',

    fetchProjectListInit: 'FETCH_PROJECT_LIST_INIT',
    fetchProjectListSuccess: 'FETCH_PROJECT_LIST_SUCCESS',
    fetchProjectListError: 'FETCH_PROJECT_LIST_ERROR',

    fetchProjectInit: 'FETCH_PROJECT_INIT',
    fetchProjectSuccess: 'FETCH_PROJECT_SUCCESS',
    fetchProjectError: 'FETCH_PROJECT_ERROR',

    createProjectInit: 'CREATE_PROJECT_INIT',
    createProjectSuccess: 'CREATE_PROJECT_SUCCESS',
    createProjectError: 'CREATE_PROJECT_ERROR',

    updateProjectInit: 'UPDATE_PROJECT_INIT',
    updateProjectSuccess: 'UPDATE_PROJECT_SUCCESS',
    updateProjectError: 'UPDATE_PROJECT_ERROR',

    deleteProjectInit: 'DELETE_PROJECT_INIT',
    deleteProjectSuccess: 'DELETE_PROJECT_SUCCESS',
    deleteProjectError: 'DELETE_PROJECT_ERROR',


    fetchTaskListInit: 'FETCH_TASK_LIST_INIT',
    fetchTaskListSuccess: 'FETCH_TASK_LIST_SUCCESS',
    fetchTaskListError: 'FETCH_TASK_LIST_ERROR',

    createTaskInit: 'CREATE_TASK_INIT',
    createTaskSuccess: 'CREATE_TASK_SUCCESS',
    createTaskError: 'CREATE_TASK_ERROR',

    updateTaskInit: 'UPDATE_TASK_INIT',
    updateTaskSuccess: 'UPDATE_TASK_SUCCESS',
    updateTaskError: 'UPDATE_TASK_ERROR',

    deleteTaskInit: 'DELETE_TASK_INIT',
    deleteTaskSuccess: 'DELETE_TASK_SUCCESS',
    deleteTaskError: 'DELETE_TASK_ERROR',

    fetchCommentListInit: 'FETCH_COMMENT_LIST_INIT',
    fetchCommentListSuccess: 'FETCH_COMMENT_LIST_SUCCESS',
    fetchCommentListError: 'FETCH_COMMENT_LIST_ERROR',

    uploadDocumentInit: 'UPLOAD_DOCUMENT_INIT',
    uploadDocumentSuccess: 'UPLOAD_DOCUMENT_SUCCESS',
    uploadDocumentError: 'UPLOAD_DOCUMENT_ERROR',

}

export default actionTypes;