import { API_ENDPOINT } from '../helpers/constants';
import actionTypes from './actionTypes';
import { hideError, makeAuthenticatedGetCall } from './commonActions';


export const fetchUserInit = () => ({
	type: actionTypes.fetchUserInit,
});

export const fetchUserSuccess = (payoutData) => ({
	type: actionTypes.fetchUserSuccess,
	payload: payoutData,
});

export const  fetchUserError = (error) => ({
	type: actionTypes.fetchUserError,
	error: JSON.stringify(error.toString()),
});

export const setupUser  = (query) => (dispatch) => {
	dispatch(fetchUserInit());
	return makeAuthenticatedGetCall( `${API_ENDPOINT}/user/`, query)
		.then(res => res.json())
		.then(json => {
			if(json.code === 200) {
				dispatch(fetchUserSuccess(json.response))
			} else{
				dispatch(fetchUserError(json.message))
				setTimeout(() => dispatch(hideError()), 2000);
				
			}
		}).catch(error=> {
			console.log('Error Getting Fetching User Details', error);
			dispatch(fetchUserError(error))
			setTimeout(() => dispatch(hideError()), 2000);
		})
	
}


