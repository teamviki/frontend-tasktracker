import actionTypes from './actionTypes';
import {API_ENDPOINT} from '../helpers/constants';
import { hideError, makeAuthenticatedGetCall, makeAuthenticatedPostCall, makeAuthenticatedPutCall, makeAuthenticatedDeleteCall } from './commonActions';


export const fetchProjectListInit = () => ({
    type: actionTypes.fetchProjectListInit
})

export const fetchProjectListError = (error) => ({
    type: actionTypes.fetchProjectListError,
    error: JSON.stringify(error.toString()),
})

export const fetchProjectListSuccess = (data) => ({
    type: actionTypes.fetchProjectListSuccess,
    payload: data,
})

export const fetchProjectListData  = (queryParam) => (dispatch) => {
   dispatch(fetchProjectListInit())
   return makeAuthenticatedGetCall(`${API_ENDPOINT}/projects/`)
          .then(res => res.json())
          .then(json => {
			if(json.code === 200) {
				dispatch(fetchProjectListSuccess(json.response))
			} else {
				dispatch(fetchProjectListError(json.message))
				setTimeout(() => dispatch(hideError()), 2000);
            }
        }).catch(error => {
            dispatch(fetchProjectListError(error))
            setTimeout(() => dispatch(hideError()), 2000);

        })
};

export const createProjectInit = () => ({
    type: actionTypes.createProjectInit
})

export const createProjectError = (error) => ({
    type: actionTypes.createProjectError,
    error: JSON.stringify(error.toString()),
})

export const createProjectSuccess = (data) => ({
    type: actionTypes.createProjectSuccess,
    payload: data,
})

export const createNewProject = (formData) => dispatch => {
    dispatch(createProjectInit())
    return makeAuthenticatedPostCall(`${API_ENDPOINT}/project/create`, formData)
    .then(res => res.json())
    .then(json => {
        if(json.code === 200) {
            dispatch(createProjectSuccess(json.response))
            dispatch(fetchProjectListData())
        } else {
            dispatch(createProjectError(json.message))
            setTimeout(() => dispatch(hideError()), 2000);
        }
    }).catch(error => {
        dispatch(createProjectError(error))
        setTimeout(() => dispatch(hideError()), 2000);

    })
}



export const updateProjectInit = () => ({
    type: actionTypes.updateProjectInit
})

export const updateProjectError = (error) => ({
    type: actionTypes.updateProjectError,
    error: JSON.stringify(error.toString()),
})

export const updateProjectSuccess = (data) => ({
    type: actionTypes.updateProjectSuccess,
    payload: data,
})

export const updateExistingProject = (formData, projectId) => dispatch => {
    dispatch(updateProjectInit())
    return makeAuthenticatedPutCall(`${API_ENDPOINT}/project/update/${projectId}`, formData)
    .then(res => res.json())
    .then(json => {
        if(json.code === 200) {
            dispatch(updateProjectSuccess(json.response))
            dispatch(fetchProjectListData())
        } else {
            dispatch(updateProjectError(json.message))
            setTimeout(() => dispatch(hideError()), 2000);
        }
    }).catch(error => {
        dispatch(updateProjectError(error))
        setTimeout(() => dispatch(hideError()), 2000);

    })
}



export const deleteProjectInit = () => ({
    type: actionTypes.updateProjectInit
})

export const deleteProjectError = (error) => ({
    type: actionTypes.deleteProjectError,
    error: JSON.stringify(error.toString()),
})

export const deleteProjectSuccess = (data) => ({
    type: actionTypes.deleteProjectSuccess,
    payload: data,
})

export const deleteExistingProject = (projectId) => dispatch => {
    dispatch(deleteProjectInit())
    return makeAuthenticatedDeleteCall(`${API_ENDPOINT}/project/delete/${projectId}`)
    .then(res => res.json())
    .then(json => {
        if(json.code === 200) {
            dispatch(deleteProjectSuccess(json.response))
            dispatch(fetchProjectListData())
        } else {
            dispatch(deleteProjectError(json.message))
            setTimeout(() => dispatch(hideError()), 2000);
        }
    }).catch(error => {
        dispatch(deleteProjectError(error))
        setTimeout(() => dispatch(hideError()), 2000);

    })
}
