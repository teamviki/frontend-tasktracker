import React, { Component, Fragment } from 'react';
import { bindActionCreators } from "redux";
import { connect } from 'react-redux';
import { withStyles, Modal, DialogContent } from '@material-ui/core';
import * as projectActions from '../../actions/project';
import * as taskActions from '../../actions/task';
import ProjectTable from '../../components/Project/ProjectTable';
import ProjectForm from '../../components/Project/ProjectForm';

import CircularProgress from '@material-ui/core/CircularProgress';
import SearchForm from '../../components/SearchForm/SearchForm';

const styles = theme => ({
    selectorCard: {
        padding: theme.spacing.unit * 1.5,
        margin: theme.spacing.unit * 1.5,
        marginBottom: 0
    }
})
class Project extends Component {
    componentDidMount() {
        this.props.setTitle('Projects');
        this.props.fetchProjectListData();
    }
    state = {
        modalOpen: false,
        selectedProject: this.props.project
    }
    
    updateModalView = (value) => {
        this.setState({modalOpen: value})
    }
  
    setSelectedProject = item => {
        console.log('set Selected',item)
        this.setState({selectedProject: item})
    }
    render() {
        const { classes, error, setModalMessage, } = this.props
        if (error) {
            setModalMessage(error)
        } else {
            setModalMessage(null)
        }
        console.log(this.props)
        return (
            <div>
                <div style={{ padding: 8 * 3, paddingBottom:0 }}>
                <SearchForm 
                  submitCallback={this.props.fetchProjectListData}
                  statusList={['CREATED', 'PENDING', 'PAID', 'ALL']}
                  handleCreate={this.updateModalView}
                  createItemName="Project"

                />
                </div>
                {this.props.loading ? <CircularProgress
                    style={{ margin: '10% 45%' }} />
                    : <Fragment>
                        <div className={classes.selectorCard} style={{ padding: 16, paddingTop: 0  }}>
                        
                            <Modal open={this.state.modalOpen} className="modal-create">
                                <DialogContent style={{padding: 0}}>
                                <ProjectForm  closeForm={this.updateModalView}
                                    createNewProject={this.props.createNewProject}
                                    selectedProject={this.state.selectedProject}
                                    updateProject={this.props.updateExistingProject}
                                />
                                </DialogContent>
                            </Modal>
                            <ProjectTable 
                                dataSet={this.props.projectList}
                                setSelectedProject={this.setSelectedProject}
                                updateProject={this.props.updateExistingProject}
                                editModal={this.updateModalView}
                                deleteProject={this.props.deleteExistingProject}
                                createTask={this.props.createNewTask}
                                deleteTask={this.props.deleteExistingTask}
                                updateTask={this.props.updateExistingTask}

                            />
                        </div>
                    </Fragment>}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    ...state.project,
    ...state.task
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        ...projectActions,
        ...taskActions,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Project))
