import React from 'react'
import { Auth } from '../helpers/auth';
import {Redirect} from 'react-router-dom';

export default function Logout() {
    Auth.clearToken();
  return (
    <Redirect to="/login" />
  )
}
