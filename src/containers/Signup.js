import React, { Component } from 'react'
import { Grid } from '@material-ui/core';
import SignUpCard from '../components/Signup/SignUpCard';
export class Signup extends Component {
  render() {
    return (
      <div>
          <Grid container className="login-container">
            <Grid item xs={12} sm={6} md={4}>
              <SignUpCard history={this.props.history} />
            </Grid>
          </Grid>
       
      </div>
    )
  }
}

export default Signup;
