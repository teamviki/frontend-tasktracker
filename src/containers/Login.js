import React, { Component } from 'react'
import { Grid } from '@material-ui/core';
import LoginCard from '../components/Login/LoginCard';
export class Login extends Component {
  render() {
    return (
      <div>
          <Grid container className="login-container">
            <Grid item xs={12} sm={6} md={4}>
              <LoginCard history={this.props.history} />
            </Grid>
          </Grid>
       
      </div>
    )
  }
}

export default Login;
