import React, { Component } from 'react';
import { Provider } from 'react-redux'

import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import store from '../store';
import '../assets/styles/app.scss';
import Login from './Login';
import Logout from './Logout';

import CssBaseline from '@material-ui/core/CssBaseline';

import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import MomentUtils from '@date-io/moment';
import AuthenticatedRoute  from './AuthenticatedRoute';
import Signup  from './Signup';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <CssBaseline />
          <Router>
          <Switch>
           <Route path="/signup" exact component={Signup} />

            <Route path="/login" exact component={Login} />
            <Route path="/logout" exact component={Logout} />

            <Route path="/"  component={AuthenticatedRoute} >
            </Route>
            <Route render={() => 'Not Found'} />

          </Switch>
          </Router>

        </MuiPickersUtilsProvider>
      </Provider>

    );
  }
}
export default App;
