import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import Header from '../components/Header/Header';
import * as appActions from '../actions/app';
import {CircularProgress} from '@material-ui/core';


export class AuthenticatedRoute extends Component {
    componentDidMount() {
        this.props.setupUser()

        console.log('prop',this.props)
    }
    redirectToLogin = (error) => {
            this.props.history.push({ pathname: '/login', state:{error: error}})
    }
    render() {
        const {user, isLoading, error} = this.props;
        return (
            <Fragment >
                {!!isLoading && <CircularProgress
                    style={{ margin: '10% 45%' }}
                />  }
                {
                    !!user &&   <Header user={user}/>
                }
                {!!error &&  this.redirectToLogin(error)}
            </Fragment >

        )
    }
}

const mapStateToProps = (state) => ({
    ...state.app
})

const mapDispatchToProps = (dispatch) => {    
   return bindActionCreators(appActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticatedRoute)
