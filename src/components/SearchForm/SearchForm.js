import React, { Component } from "react";
// import { DatePicker } from "material-ui-pickers";
import {
  Card,
  withStyles,
  Button,
  Grid,
  Select,
  MenuItem,
  InputLabel
} from "@material-ui/core";

const styles = theme => ({
  selectorCard: {
    padding: theme.spacing.unit * 1.5
  }
});

class SearchForm extends Component {
  state = {
    fromDate: null,
    toDate: null,
    status: this.props.defaultStatus || "ALL",
    loading: false
  };

  handleFromDateChange = date => {
    this.setState({ fromDate: date });
  };

  handleToDateChange = date => {
    this.setState({ toDate: date });
  };
  handleInputChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = () => {
    const fromDate = this.state.fromDate
      ? this.state.fromDate.format("DD/MM/YYYY")
      : null;
    const toDate = this.state.toDate
      ? this.state.toDate.format("DD/MM/YYYY")
      : null;
    const form = {};
    if (fromDate) form.fromDate = fromDate;
    if (toDate) form.toDate = toDate;
    form.status = this.state.status;
    this.props.submitCallback(form);
  };
  render() {
    const { classes } = this.props;
    const { 
        // fromDate,
        // toDate, 
        status, 
        loading } = this.state;
    return (
      <Card className={classes.selectorCard}>
        <Grid container>
          {/* <Grid item xs={6} sm={2}>
            <DatePicker
                keyboard
                clearable
                label="From Date"
                value={fromDate}
                onChange={this.handleFromDateChange}
                animateYearScrolling={false}
                autoOk
                format="MM/DD/YYYY"
                maxDate={new Date()}
                onInputChange={e => console.log("Keyboard Input:", e.target.value)}
            /> </Grid>
        <Grid item xs={6} sm={2}>
            <DatePicker
                keyboard
                clearable
                label="To Date"
                value={toDate}
                onChange={this.handleToDateChange}
                animateYearScrolling={false}
                maxDate={new Date()}
                format="MM/DD/YYYY"
                autoOk
                onInputChange={e => console.log("Keyboard Input:", e.target.value)}
            /> </Grid> */}
          <Grid item xs={6} sm={2}>
            <InputLabel htmlFor="status">Status</InputLabel>
            <br />
            <Select
              value={status}
              onChange={this.handleInputChange}
              name="status"
              fullWidth
            >
              {/* <MenuItem value="" disabled>Status</MenuItem> */}
              {this.props.statusList.map(item => (
                <MenuItem value={item} key={item}>
                  {item}
                </MenuItem>
              ))}
              {/* <MenuItem value="PENDING" selected>PENDING</MenuItem>
            <MenuItem value="PAID">PAID</MenuItem>
            <MenuItem value="ALL">ALL</MenuItem> */}
            </Select>
          </Grid>
          <Grid
            item
            xs={6}
            sm={2}
            style={{ paddingLeft: 30, paddingTop: 10, paddingRight: 30 }}
          >
            <Button
              color="primary"
              variant="contained"
              fullWidth
              disabled={loading}
              onClick={this.handleSubmit}
            >
              {" "}
              Find{" "}
            </Button>{" "}
          </Grid>

          <Grid
            item
            xs={12}
            sm={2}
            style={{ paddingLeft: 30, paddingTop: 10, paddingRight: 30 }}
          >
            <Button
              color="primary"
              variant="contained"
              fullWidth
              onClick={() => this.props.handleCreate(true)}
            >
              Create {this.props.createItemName}
            </Button>
          </Grid>
        </Grid>
      </Card>
    );
  }
}
export default withStyles(styles)(SearchForm);
