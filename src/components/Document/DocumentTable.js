import React, { Component, Fragment } from "react";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  TextField,
  Grid,
  Button,
  IconButton
} from "@material-ui/core";
import { Formik } from "formik";
import { Delete } from "@material-ui/icons";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { API_ENDPOINT, MEDIA_ENDPOINT } from "../../helpers/constants";
import { Auth } from "../../helpers/auth";
import * as actions from "../../actions/project";

class DocumentTable extends Component {
  state = { isUploading: false };
  handleFormSubmit = (formData, actions) => {
    const form = new FormData();
    form.append("file", formData.file);
    form.append("taskId", formData.taskId);
    this.setState({ isUploading: true });
    fetch(`${API_ENDPOINT}/document/create`, {
      method: "POST",
      headers: { Authorization: `Token ${Auth.getToken()}` },
      body: form
    })
      .then(res => res.json())
      .then(data => {
        this.setState({ isUploading: false });
      this.props.fetchProjectListData();

      })
      .catch(err => {
        console.log("Document Upload Error", err);
        this.setState({ isUploading: false });
      });
  };

  handleDocumentDelete = documentId => {
    fetch(`${API_ENDPOINT}/document/delete/${documentId}`, {
      method: "DELETE",
      headers: { Authorization: `Token ${Auth.getToken()}` },
    })
      .then(res => res.json())
      .then(data => {
        this.setState({ isUploading: false });
      this.props.fetchProjectListData();

      })
      .catch(err => {
        console.log("Document Delete Error", err);
        this.setState({ isUploading: false });
      });
  };
  render() {
    return (
      <Fragment>
        <Grid container className="task-table">
          <Formik
            initialValues={{
              file: undefined,
              taskId: this.props.taskId
            }}
            onSubmit={this.handleFormSubmit}
            render={props => (
              <Fragment>
                <Grid item xs={6} sm={6} className="padding-y-10">
                  <TextField
                    type="file"
                    fullWidth
                    name="file"
                    label="Document"
                    onBlur={props.handleBlur}
                    onChange={event => {
                      props.setFieldValue("file", event.currentTarget.files[0]);
                    }}
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </Grid>
                <Grid item xs={6} sm={6} className="padding-y-10">
                  <Button
                    color="primary"
                    variant="contained"
                    onClick={props.handleSubmit}
                    className="create-task-button"
                    fullWidth
                    disabled={this.setState.isUploading}
                  >
                    Upload Document
                  </Button>
                </Grid>
              </Fragment>
            )}
          />
          <Grid item xs={12}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Id</TableCell>
                  <TableCell colSpan={4}>Document Link</TableCell>
                  <TableCell colSpan={3}>Options</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.props.dataSet &&
                  this.props.dataSet.map(item => {
                    return (
                      <Fragment>
                        <TableCell>{item.id}</TableCell>
                        <TableCell colSpan={4}>
                          <a target="_blank" rel="noreferrer noopener"  href={`${MEDIA_ENDPOINT}${item.file}`}>
                            {item.file}
                          </a>
                        </TableCell>
                        <TableCell colSpan={3}>
                          <IconButton color="primary" onClick={() => this.handleDocumentDelete(item.id)}>
                            <Delete />
                          </IconButton>
                        </TableCell>
                      </Fragment>
                    );
                  })}
              </TableBody>
            </Table>
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions
    },
    dispatch
  );
}
export default connect(
  null,
  mapDispatchToProps
)(DocumentTable);
