import React, { Fragment, useState } from "react";
import {
  TableCell,
  TableRow,
  IconButton,
  TextField,
  Select,
  MenuItem,
  InputLabel,
  Input,
  Collapse
} from "@material-ui/core";
import { Formik } from "formik";
import { Delete, Edit, Check } from "@material-ui/icons";
import clsx from "clsx";
import DocumentTable from "../Document/DocumentTable";

export default function TaskDetail(props) {
  const [formEditable, setFormEditable] = useState(false);
  const [expanded, setExpanded] = useState(false);

  const handleDeleteTask = taskId => {
    props.deleteTask(taskId);
  };

  const handleUpdateTask = (formData, actions) => {
    actions.setSubmitting(true);
    props
      .updateTask(formData, formData.id)
      .then(data => actions.setSubmitting(false))
      .catch(err => actions.setSubmitting(false));
  };
  return (
    <Fragment>
      <TableRow key={props.task.id}>
        <Fragment>
          <TableCell style={{ display: formEditable ? "none" : "table-cell" }}  onClick={() => setExpanded(!expanded)}>
            {props.task.id}
          </TableCell>
          <TableCell
            colSpan={4}
            style={{ display: formEditable ? "none" : "table-cell" }}
            onClick={() => setExpanded(!expanded)}
          >
            {props.task.name}
          </TableCell>
          <TableCell
            colSpan={1}
            style={{ display: formEditable ? "none" : "table-cell" }}
            onClick={() => setExpanded(!expanded)}
          >
            {props.task.priority}
          </TableCell>
          <TableCell
            colSpan={2}
            style={{ display: formEditable ? "none" : "table-cell" }}
            onClick={() => setExpanded(!expanded)}
          >
            {props.task.end_date
              ? new Date(props.task.end_date).toLocaleDateString()
              : ""}
          </TableCell>
          <TableCell
            colSpan={1}
            style={{ display: formEditable ? "none" : "table-cell" }}
            onClick={() => setExpanded(!expanded)}
          >
            {props.task.status}
          </TableCell>
        </Fragment>
        <Fragment>
          <Formik
            initialValues={{
              id: props.task.id,
              name: props.task.name,
              priority: props.task.priority,
              endDate: props.task.endDate,
              status: props.task.status
            }}
            onSubmit={handleUpdateTask}
            render={formikProps => {
              return (
                <Fragment>
                  <TableCell
                    style={{ display: !formEditable ? "none" : "table-cell" }}
                  >
                    <IconButton
                      onClick={formikProps.handleSubmit}
                      disabled={formikProps.isSubmitting}
                      color="primary"
                    >
                      <Check />
                    </IconButton>
                  </TableCell>

                  <TableCell
                    colSpan={4}
                    style={{ display: !formEditable ? "none" : "table-cell" }}
                  >
                    <TextField
                      onChange={formikProps.handleChange}
                      onBlur={formikProps.handleBlur}
                      name="name"
                      value={formikProps.values.name}
                      label="Task Name"
                      required
                    />
                  </TableCell>
                  <TableCell
                    colSpan={1}
                    style={{ display: !formEditable ? "none" : "table-cell" }}
                  >
                    <InputLabel
                      shrink
                      htmlFor={`${formikProps.values.id}-select`}
                    >
                      Priority
                    </InputLabel>
                    <Select
                      label="Priority"
                      onBlur={formikProps.handleBlur}
                      onChange={formikProps.handleChange}
                      name="priority"
                      displayEmpty
                      fullWidth
                      input={
                        <Input
                          name="priority"
                          id={`${formikProps.values.id}-select`}
                        />
                      }
                      value={formikProps.values.priority}
                    >
                      <MenuItem value="">None</MenuItem>
                      <MenuItem value="High">High</MenuItem>
                      <MenuItem value="Moderate">Moderate</MenuItem>
                      <MenuItem value="Low">Low</MenuItem>
                    </Select>
                  </TableCell>
                  <TableCell
                    colSpan={2}
                    style={{ display: !formEditable ? "none" : "table-cell" }}
                  >
                    <TextField
                      type="date"
                      fullWidth
                      name="endDate"
                      label="Deadline"
                      onBlur={formikProps.handleBlur}
                      onChange={formikProps.handleChange}
                      InputLabelProps={{
                        shrink: true
                      }}
                      value={formikProps.values.endDate}
                    />
                  </TableCell>
                  <TableCell
                    colSpan={1}
                    style={{ display: !formEditable ? "none" : "table-cell" }}
                  >
                    <InputLabel
                      shrink
                      htmlFor={`${formikProps.values.id}-status`}
                    >
                      Status
                    </InputLabel>
                    <Select
                      label="Status"
                      onBlur={formikProps.handleBlur}
                      onChange={formikProps.handleChange}
                      name="status"
                      fullWidth
                      input={
                        <Input
                          name="status"
                          id={`${formikProps.values.id}-status`}
                        />
                      }
                      value={formikProps.values.status}
                    >
                      <MenuItem value="CREATED">Created</MenuItem>
                      <MenuItem value="PENDING">Pending</MenuItem>
                      <MenuItem value="COMPLETE">Complete</MenuItem>
                    </Select>
                  </TableCell>
                </Fragment>
              );
            }}
          />
        </Fragment>

        <TableCell colSpan={3}>
          <IconButton onClick={() => setFormEditable(!formEditable)}>
            <Edit />
          </IconButton>
          <IconButton onClick={() => handleDeleteTask(props.task.id)}>
            <Delete />
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow
        className={clsx(!expanded && "rowHidden")}
        style={{ backgroundColor: "#ddd" }}
      >
        <TableCell colSpan={9} className={clsx(!expanded && "rowHidden")}>
          <Collapse in={expanded} timeout="auto" unmountOnExit >
          <DocumentTable taskId={props.task.id} dataSet={props.task.documents}/>
          </Collapse>
        </TableCell>
      </TableRow>
    </Fragment>
  );
}
