import React, { Fragment } from "react";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  TextField,
  Grid,
  Select,
  MenuItem,
  InputLabel,
  Input,
  Button,
} from "@material-ui/core";
import { Formik } from "formik";
import TaskDetail from './TaskDetail';

export default function Task(props) {
    const handleFormSubmit = (formdata, actions) => {
        props.createTask(formdata)
    }
  const handleDeleteTask =  props.deleteTask;
  const handleUpdateTask = props.updateTask
    
  return (
    <Fragment>
      <Grid container className="task-table">
        <Formik
          initialValues={{
            taskName: "",
            priority: "",
            projectId: props.projectId,
            endDate: undefined
          }}
          onSubmit={handleFormSubmit}
          render={props => (
            <Fragment>
              <Grid item xs={12} sm={6} className="padding-y-10">
                <TextField
                  onChange={props.handleChange}
                  onBlur={props.handleBlur}
                  name="taskName"
                  label="Task Name"
                  value={props.values.taskName}
                  fullWidth
                />
              </Grid>
              <Grid item xs={4} sm={2} className="padding-y-10">
                
                <InputLabel shrink htmlFor={`${props.values.projectId}-select`}>
                  Priority
                </InputLabel>
                <Select
                  label="Priority"
                  onBlur={props.handleBlur}
                  onChange={props.handleChange}
                  name="priority"
                  displayEmpty
                  fullWidth
                  input={
                    <Input
                      name="priority"
                      id={`${props.values.projectId}-select`}
                    />
                  }
                  value={props.values.priority}
                >
                  <MenuItem value="">None</MenuItem>
                  <MenuItem value="High">High</MenuItem>
                  <MenuItem value="Moderate">Moderate</MenuItem>
                  <MenuItem value="Low">Low</MenuItem>
                </Select>
              </Grid>
              <Grid item xs={4} sm={2} className="padding-y-10">
                <TextField
                type="date"
                fullWidth
                name="endDate"
                label="Deadline"
                onBlur={props.handleBlur}
                onChange={props.handleChange}
                InputLabelProps={{
                    shrink: true,
                  }}
                value={props.values.endDate}
                />
                
              </Grid>
              <Grid item xs={4} sm={2} className="padding-y-10">
                <Button
                  color="primary"
                  variant="contained"
                  onClick={props.handleSubmit}
                  className="create-task-button"
                  fullWidth
                >
                  Create Task
                </Button>
              </Grid>
            </Fragment>
          )}
        />
        <Grid item xs={12}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell colSpan={4}>Task Name</TableCell>
                <TableCell colSpan={1}>Priority</TableCell>
                <TableCell colSpan={2}>Deadline</TableCell>
                <TableCell colSpan={1}>Status</TableCell>
                <TableCell colSpan={3}>Options</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {props.dataSet &&
                props.dataSet.map(item => {
                  return (
                    <TaskDetail
                      key={item.id}
                      deleteTask = {handleDeleteTask}
                      updateTask = {handleUpdateTask}
                      task={item}
                    />
                  );
                })}
            </TableBody>
          </Table>
        </Grid>
      </Grid>
    </Fragment>
  );
}
