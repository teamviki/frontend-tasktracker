import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Card, TextField, Button, FormControl, Divider } from "@material-ui/core";
import { Formik } from "formik";
import * as Yup from "yup";
import * as actions from "../../actions/project";

const projectSchema = Yup.object().shape({
  name: Yup.string().required(),
  description: Yup.string().required(),
  id: Yup.number(),
});

class ProjectForm extends Component {
  handleCreateProject = (values, actions) => {
    actions.setSubmitting(true)
    this.props.createNewProject(values)
    .then(data => {
      actions.setSubmitting(false);
      this.props.closeForm(false);
    })
    .catch(err => {
      console.log('Error Submitting Project Details', err)
      actions.setSubmitting(false);
      this.props.closeForm(false);
    });
  }
  handleUpdateProject = (values, actions) => {
    actions.setSubmitting(true)
    this.props.updateExistingProject(values, values.id)
    .then(data => {
      actions.setSubmitting(false);
      this.props.closeForm(false);
    })
    .catch(err => {
      console.log('Error Submitting Project Details', err)
      actions.setSubmitting(false);
      this.props.closeForm(false);
    });
  }
  render() {
    return (
      <div>
        <Card className="create-card">
          <Formik
            initialValues={{
              name: this.props.selectedProject ? this.props.selectedProject.name : undefined,
              description:  this.props.selectedProject ? this.props.selectedProject.description : undefined,
              id:  this.props.selectedProject ? this.props.selectedProject.id : undefined,
              startDate: undefined,
              endDate: undefined
            }}
            validationSchema={projectSchema}
            onSubmit={ this.props.selectedProject ?  this.handleUpdateProject : this.handleCreateProject}
            render={props => (
              <form onSubmit={props.handleSubmit}>
                <FormControl fullWidth>
                  <TextField
                    id="name"
                    label="Name"
                    error={!!props.errors.name}
                    value={props.values.name}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    margin="normal"
                    fullWidth
                    required
                  />
                  {props.errors.name && (
                    <div id="feedback">{props.errors.name}</div>
                  )}
                </FormControl>
                <FormControl fullWidth style={{ marginBottom: 50 }}>
                  <TextField
                    id="description"
                    label="Description"
                    error={!!props.errors.description}
                    value={props.values.description}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    type="description"
                    margin="normal"
                    name="description"
                    fullWidth
                    rows="3"
                    required
                  />
                  {props.errors.description && (
                    <div id="feedback">{props.errors.description}</div>
                  )}
                </FormControl>
                {/* <FormControl fullWidth style={{ marginBottom: 50 }}>
                  <Field
                    component={FormikDatePicker}
                    fieldName="startDate"
                    label="From Date"
                    value={props.values.startDate}
                  />

                  {props.errors.startDate && (
                    <div id="feedback">{props.errors.startDate}</div>
                  )}
                </FormControl>
                <FormControl fullWidth style={{ marginBottom: 50 }}>
                  <Field
                    component={FormikDatePicker}
                    fieldName="endDate"
                    label="End Date"
                    setFieldValue = {props.setFieldValue}
                  />
                  {props.errors.endDate && (
                    <div id="feedback">{props.errors.endDate}</div>
                  )}
                </FormControl> */}
                <FormControl fullWidth>
                  <Button
                    onClick={props.handleSubmit}
                    variant="contained"
                    color="primary"
                    fullWidth
                    disabled={props.isSubmitting}
                  >
                    {props.values.id ? 'Update' : 'Create'}
                  </Button>
                </FormControl>
                <Divider />
              <FormControl fullWidth style={{marginTop: 20}}>
                  <Button
                    onClick={() => this.props.closeForm(false)}
                    variant="contained"
                    color="secondary"
                    fullWidth
                    disabled={props.isSubmitting}
                  >
                    Cancel
                  </Button>
                </FormControl>
              </form>
             
            )}
          />
          
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.project
});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions
    },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectForm);
