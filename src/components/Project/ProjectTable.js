import React from "react";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Card,
  CardContent,
} from "@material-ui/core";
import ProjectDetail from './ProjectDetail';

export default function ProjectTable(props) {
  return (
    <Card>
      <CardContent style={{overflowX: 'scroll'}}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell colSpan={2}>Project Name</TableCell>
              <TableCell colSpan={3}>Project Description</TableCell>
              <TableCell colSpan={1}>Tasks</TableCell>

              <TableCell colSpan={3}>Options</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.dataSet &&
              props.dataSet.map(item => {
                return (
                  <ProjectDetail 
                    key={item.id}
                    id={item.id}
                    name={item.name}
                    description={item.description}
                    tasks={item.tasks}
                    deleteProject={props.deleteProject}
                    createTask={props.createTask}
                    deleteTask={props.deleteTask}
                    updateTask={props.updateTask}
                    dataSet={item.tasks}
                    setSelectedProject={props.setSelectedProject}
                    editModal={props.editModal}
                  />
                );
              })}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
}
