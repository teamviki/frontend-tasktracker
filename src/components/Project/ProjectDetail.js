import React, {useState, Fragment} from 'react'
import { TableRow, TableCell, IconButton, Collapse, } from '@material-ui/core'
import { Edit, Delete } from '@material-ui/icons'
import clsx  from 'clsx';
import TaskTable from '../Task/TaskTable'


export default function ProjectDetail(props) {
    const [expanded, setExpanded] = useState(false)
    const handleUpdateProject = () => {
        const projectItem = {
            id: props.id, name: props.name, description: props.description
        }
        props.setSelectedProject(projectItem) 
        props.editModal(true)
    }
    return (
        <Fragment key={props.id}>
        <TableRow  >
          <TableCell colSpan={1} onClick={() => setExpanded(!expanded)}>{props.id}</TableCell>
          <TableCell colSpan={2} onClick={() => setExpanded(!expanded)}>{props.name}</TableCell>
          <TableCell colSpan={3} onClick={() => setExpanded(!expanded)}>{props.description}</TableCell>
          <TableCell colSpan={1} onClick={() => setExpanded(!expanded)}>{props.dataSet ? props.dataSet.length : 0}</TableCell>

          <TableCell colSpan={3}>
            <IconButton onClick={handleUpdateProject}>
              <Edit />
            </IconButton>
            <IconButton onClick={() => props.deleteProject(props.id)}>
              <Delete />
            </IconButton>
          </TableCell>
        </TableRow>
        <TableRow className={clsx(!expanded && 'rowHidden' )} style={{backgroundColor: '#eee'}}>
          <TableCell colSpan={9} className={clsx(!expanded && 'rowHidden' )}>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <TaskTable 
            projectId={props.id}
            createTask={props.createTask}
            updateTask={props.updateTask}
            deleteTask={props.deleteTask}
            dataSet={props.tasks}
          />
        </Collapse>
          </TableCell>
        </TableRow>
      </Fragment>
    )
}
