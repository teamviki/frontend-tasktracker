import React from "react";
import { DatePicker } from "material-ui-pickers";


const FormikDatePicker = ({
    fieldName,
    setFieldValue,
    field: { value },
    label,
    ...rest
  }) => {
    console.log('val', setFieldValue ,fieldName, label, value);
    return (
      <DatePicker
        name={fieldName}
        keyboard
        clearable
        autoOk
        label={label}
        format="dd/MM/yyyy"
        placeholder="10/10/2018"
        // handle clearing outside => pass plain array if you are not controlling value outside
        // mask={value =>
        //   value
        //     ? [/[0-3]/, /\d/, "/", /0|1/, /\d/, "/", /1|2/, /\d/, /\d/, /\d/]
        //     : []
        // }
        disableOpenOnEnter
        onChange={value => {
          setFieldValue(fieldName, value);
        }}
        value={value}
        animateYearScrolling={false}
      />
    );
  };

export default FormikDatePicker;
  