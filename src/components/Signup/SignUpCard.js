import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Card,
  TextField,
  Button,
  FormControl,
  Snackbar
} from "@material-ui/core";
import { Formik } from "formik";
import { Link } from "react-router-dom";
import { makePostCall } from "../../actions/commonActions";
// import { Auth } from "../../helpers/auth";
import { API_ENDPOINT } from "../../helpers/constants";
// import  * as appActionTypes
const validate = values => {
  let errors = {};

  if (!values.email) {
    errors.email = "Required";
  } else if (values.email) {
    delete errors.password;
  }

  if (!values.password) {
    errors.password = "Required";
  } else if (values.password) {
    delete errors.password;
  }
  return errors;
};

export class LoginCard extends Component {
  state = {
    error: false,
    errorMessage: null
  };

  handleSignup = (form, actions) => {
    actions.setSubmitting(true);
    makePostCall(`${API_ENDPOINT}/signup/`, form)
      .then(res => res.json())
      .then(async json => {
        actions.setSubmitting(false);
        if (json.code === 200) {
          alert('Account Created Successfully Login to Continue')
          this.props.history.push("/login");
        } else {
          this.setState({ errorMessage: json.response, error: true });
        }
      })
      .catch(error => {
        this.setState({ error: true });
        actions.setSubmitting(false);
      });
  };

  handleSnackBarClose = () => this.setState({ error: false });

  render() {
    return (
      <div style={{padding: 15}}>
        <Card className="login-card">
          {/* <img src={Logo}  alt="sprouts-logo" style={{height:150, margin: 'auto',  padding:'30px', display:'block'}}/> */}

          <Formik
            initialValues={{
              email: "",
              password: ""
            }}
            validate={validate}
            onSubmit={(values, actions) => this.handleSignup(values, actions)}
            render={props => (
              <form onSubmit={props.handleSubmit}>
                <FormControl fullWidth>
                  <TextField
                    id="email"
                    label="Email"
                    error={!!props.errors.email}
                    value={props.values.email}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    margin="normal"
                    fullWidth
                    required
                    type="email"
                  />
                  {props.errors.email && (
                    <div id="feedback">{props.errors.email}</div>
                  )}
                </FormControl>
                <FormControl fullWidth style={{ marginBottom: 50 }}>
                  <TextField
                    id="standard-password-input"
                    label="Password"
                    error={!!props.errors.password}
                    value={props.values.password}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    type="password"
                    autoComplete="current-password"
                    margin="normal"
                    name="password"
                    fullWidth
                    required
                  />
                  {props.errors.password && (
                    <div id="feedback">{props.errors.password}</div>
                  )}
                </FormControl>
                <FormControl fullWidth>
                  <Button
                    onClick={props.handleSubmit}
                    variant="contained"
                    color="primary"
                    fullWidth
                    disabled={props.isSubmitting}
                  >
                    Signup
                  </Button>
                </FormControl>
              </form>
            )}
          />
          <FormControl fullWidth style={{ marginTop: 20 }}>
           
            <Link to="/login" style={{textDecoration: 'none'}}> <Button variant="contained" color="secondary" fullWidth>Login</Button></Link>
            
          </FormControl>
        </Card>
        {!!this.state.error && (
          <Snackbar
            message={"Something Went Wrong"}
            place="bottom"
            autoHideDuration={4000}
            open={!!this.state.error}
            onClose={this.handleSnackBarClose}
          />
        )}

        {!!this.state.errorMessage && (
          <Snackbar
            message={this.state.errorMessage}
            place="bottom"
            autoHideDuration={4000}
            open={!!this.state.error}
            onClose={this.handleSnackBarClose}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.app
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginCard);
