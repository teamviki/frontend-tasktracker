import React, { useState, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import {
  Drawer,
  AppBar,
  Toolbar,
  List,
  Typography,
  Snackbar,
  Divider,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton
} from "@material-ui/core";
import { Link, Route, Switch, Redirect } from "react-router-dom";

import PaymentIcon from "@material-ui/icons/Payment";
import WorkOff from "@material-ui/icons/WorkOff";
import MenuIcon from "@material-ui/icons/Menu";
import clsx from "clsx";

import { connect } from "react-redux";
import { withRouter } from "react-router";
import Project from "../../containers/Project/Project";

const drawerWidth = 200;
const styles = theme => ({
  root: {
    display: "absolute"
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1
  },
  menuButton: {
    marginRight: theme.spacing.unit
  },
  hide: {
    display: "none",
    transition: theme.transitions.create(['display', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
    })
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1
  },
  toolbar: theme.mixins.toolbar,
  flexListItem: {
    width: "100%",
    display: "flex",
    textDecoration: "none"
  }
});

function HeaderBase(props) {
  const { classes } = props;
  const [title, setTitle] = useState("Project");
  const [modalMessage, setModalMessage] = useState("hello");
  const [open, setOpen] = React.useState(false);

  function handleDrawerOpen() {
    setOpen(!open);
  }

  // const
  const handleSnackBarClose = (event, reason) => {
    setModalMessage(null);
  };
  return (
    <div className={classes.root}>
      {!props.user ? (
        <Redirect to="/login" />
      ) : (
        <Fragment>
          <AppBar position="fixed" className={classes.appBar}>
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                className={clsx(classes.menuButton)}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" noWrap>
                {title}
              </Typography>
            </Toolbar>
          </AppBar>
          <Drawer
            className={clsx(classes.drawer, !open && classes.hide)}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper
            }}
            anchor="left"
          >
            <div className={classes.toolbar} />
            <Divider />
            <List>
              <ListItem button>
                <Link to={`/projects`} className={classes.flexListItem}>
                  <ListItemIcon>
                    <PaymentIcon />
                  </ListItemIcon>
                  <ListItemText primary="Projects" />
                </Link>
              </ListItem>
              {/* <ListItem button>
                <Link to={`/tasks`} className={classes.flexListItem}>
                  <ListItemIcon>
                    <ListIcon />
                  </ListItemIcon>
                  <ListItemText primary="Tasks" />
                </Link>
              </ListItem>
              <ListItem button>
                <Link to={`/profile`} className={classes.flexListItem}>
                  <ListItemIcon>
                    <AccountCircle />
                  </ListItemIcon>
                  <ListItemText primary="Profile" />
                </Link>
              </ListItem> */}
              <ListItem button>
                <Link to={`/logout`} className={classes.flexListItem}>
                  <ListItemIcon>
                    <WorkOff />
                  </ListItemIcon>
                  <ListItemText primary="Logout" />
                </Link>
              </ListItem>
            </List>
            <Divider />
          </Drawer>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Switch>
              <Route
                path={`/`}
                exact
                render={props => (
                  <Project
                    {...props}
                    setTitle={setTitle}
                    setModalMessage={setModalMessage}
                  />
                )}
              />
              <Route 
              path={`/projects`} 
              exact 
              render={(props) => (
              <Project
              {...props} 
              setTitle={setTitle} 
              setModalMessage={setModalMessage} />)} />
				 {/*	<Route path={`/tasks`} exact render={(props) => <Transaction {...props} setTitle={setTitle} setModalMessage={setModalMessage} />} />
					<Route path={`/comments`} exact render={(props) => <AdvisorList {...props} setTitle={setTitle} setModalMessage={setModalMessage} />} />
					<Route path={`/advisorDetail/:advisorId`} exact render={(props) => <AdvisorDetails {...props} setTitle={setTitle} setModalMessage={setModalMessage} />} /> */}
              <Route render={() => "Not Found"} />
            </Switch>{" "}
            *
          </main>
          <Snackbar
            message={modalMessage || "Something Went Wrong"}
            place="bottom"
            autoHideDuration={4000}
            open={!!modalMessage}
            onClose={handleSnackBarClose}
          />
        </Fragment>
      )}
    </div>
  );
}
const mapStateToProps = state => ({
  ...state.app
});

export default withRouter(
  connect(mapStateToProps)(withStyles(styles)(HeaderBase))
);
