import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, TextField, Button, FormControl,Snackbar } from '@material-ui/core';
import { Formik } from 'formik'
import { Link } from 'react-router-dom'

import { makePostCall } from '../../actions/commonActions';
import { Auth } from '../../helpers/auth';
import { API_ENDPOINT } from '../../helpers/constants';
const validate = values => {
  let errors = {};

  if (!values.username) {
    errors.username = 'Required';
  } 
 
  else if (values.username) {
    delete errors.password

  }

  if (!values.password){
    errors.password = 'Required';
  } else if(values.password){
    delete errors.password
  }
  return errors;
};

export class LoginCard extends Component {

  state = {
    error: false,
    errorMessage: null
  }

  handleLogin = (form, actions) => {
      actions.setSubmitting(true)
      makePostCall(`${API_ENDPOINT}/login/`, form)
      .then(res => res.json())
      .then(async json => {
        actions.setSubmitting(false)
        if(json.code === 200){
         await Auth.setToken(json.response)
          this.props.history.push('/')
        } else {
          this.setState({errorMessage: json.response, error: true})
        }
      }).catch(error => {
        this.setState({error: true})
        actions.setSubmitting(false)

      })
  }

  handleSnackBarClose = () => this.setState({error: false})

  render() {
    return (
      <div style={{padding: 15}}>
        <Card className="login-card">
        
          <Formik
            initialValues= {{
              username: '',
              password: '',
            }}
            validate={validate}
            onSubmit={(values, actions) => this.handleLogin(values, actions)}
            render={props => (
              <form onSubmit={props.handleSubmit}>
                <FormControl  fullWidth>

                <TextField
                  id="username"
                  label="Username"
                  error={!!props.errors.username}
                  value={props.values.username}
                  onChange={props.handleChange}
                  onBlur={props.handleBlur}
                  margin="normal"
                  fullWidth
                  required
                />
                  {props.errors.username && <div id="feedback">{props.errors.username}</div>}
                </FormControl>
                <FormControl fullWidth style={{marginBottom:50}} >
                <TextField
                  id="standard-password-input"
                  label="Password"
                  error={!!props.errors.password}
                  value={props.values.password}
                  onChange={props.handleChange}
                  onBlur={props.handleBlur}
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                  name="password"
                  fullWidth
                  required
                />
                  {props.errors.password && <div id="feedback">{props.errors.password}</div>}

                </FormControl>
                <FormControl fullWidth >

                <Button
                  onClick={props.handleSubmit}
                  variant="contained"
                  color="primary"
                  fullWidth
                  disabled={props.isSubmitting}
                >
                  Login
                </Button>
                </FormControl>

              </form>

            )}
          />
             <FormControl fullWidth style={{ marginTop: 20 }}>
           
           <Link to="/signup" style={{textDecoration: 'none'}}> <Button variant="contained" color="secondary" fullWidth>Signup</Button></Link>
           
         </FormControl>

        </Card>
        {!!this.state.error && <Snackbar
				message={'Something Went Wrong'}
				place='bottom'
				autoHideDuration={4000}
				open={!!this.state.error}
				onClose={this.handleSnackBarClose}
      />}
      
      {!!this.state.errorMessage && <Snackbar
				message={this.state.errorMessage}
				place='bottom'
				autoHideDuration={4000}
				open={!!this.state.error}
				onClose={this.handleSnackBarClose}
      />}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  ...state.app
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(LoginCard)
