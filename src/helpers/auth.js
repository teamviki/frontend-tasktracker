export const Auth  = {
    getToken: () => {
       return localStorage['token'];
    },

    setToken: (token) => {
        localStorage.setItem('token', token);
    },

    isAuthenticated: () => {
        return localStorage.getItem['token'];
    },

    clearToken: () => {
        localStorage.removeItem('token');
    },
}